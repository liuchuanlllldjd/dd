module.exports = {
    // 一行最多最多几个字符，此时设置为120 字符
    printWidth: 120,
    // 使用 4 个空格缩进
    tabWidth: 4,
    tabs: false,
    // 行尾需要有分号
    semi: true,
    // 使用单引号
    singleQuote: true,
    // 对象的 key 仅在必要时用引号
    quoteProps: 'as-needed',
    // 大括号内的首尾需要空格
    bracketSpacing: true,
    // jsx 标签的反尖括号需要换行
    jsxBracketSameLine: false,
    // 箭头函数，只有一个参数的时候，也需要括号
    arrowParens: 'always',
    // 行尾形式 lf|crlf|cr|auto 默认lf
    endOfLine: 'auto',
};