import { defineConfig } from "vite";
import path from "path";
import uni from "@dcloudio/vite-plugin-uni";
import uniHot from "uni-pages-hot-modules";
//引入自动引入插件
import AutoImport from "unplugin-auto-import/vite";

import postCssPxToRem from "postcss-pxtorem";
//vant 组件
import { VantResolver } from "unplugin-vue-components/resolvers";
import Components from "unplugin-vue-components/vite";
uniHot.setupHotJs();
export default defineConfig({
  envDir: "./.env",
  plugins: [
    uni(),
    uniHot.createHotVitePlugin(),
    AutoImport({
      imports: ["vue"],
      dts: "src/auto-import.d.ts",
    }),
    Components({
      resolvers: [VantResolver()],
    }),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
      "*": path.resolve(""),
    },
  },
  css: {
    postcss: {
      plugins: [
        postCssPxToRem({
          rootValue: 18.75, // 1rem的大小
          propList: ["*"], // 需要转换的属性，这里选择全部都进行转换
        }),
      ],
    },
  },
});
