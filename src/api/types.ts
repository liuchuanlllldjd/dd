export interface ApiRes<T> {
  success: boolean;
  code: number;
  message: string;
  data: T;
  error: any;
}
