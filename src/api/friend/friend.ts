import { api, METHOD } from "@/utils/request";
import {
  AddFriendParm,
  AddFriendRes,
  GetStatRes,
  GetStateParams,
  GetFriendsRes,
} from "./friend.model";
// 添加好友
export async function addFriendApi(data: AddFriendParm) {
  return api<AddFriendRes>(METHOD.POST, "/friend", { data });
}
// 验证通过
export async function passFriendApi(data: AddFriendParm) {
  return api<AddFriendRes>(METHOD.POST, "/friend/passFriend", { data });
}
// 获取好友列表
export async function getFriends() {
  return api<GetFriendsRes>(METHOD.GET, "/friend");
}
//获取用户状态//申请中。。。
export async function getStateApi(params: GetStateParams) {
  return api<GetStatRes>(METHOD.GET, "/friend/getState", { params });
}
