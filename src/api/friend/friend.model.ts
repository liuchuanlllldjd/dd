import { User } from "./../user/user.model";
import { ID } from "../user/user.model";
export enum State {
  "NOT_FRIEND" = 0, //不是好友
  "APPLYING" = 1, //申请中
  "ISLYING" = 2, //有人申请
  "IS_FRIEND" = 3, //是好友
}
export interface Friends {
  friendId: number;
  id: number;
  friend: User;
}

export interface AddFriendParm {
  friendId: ID;
}

export interface AddFriendRes {}

export interface GetStateParams {
  friendId: ID;
}

export interface GetStatRes {
  state: State;
}

export interface GetFriendsRes {
  items: Friends[];
}
