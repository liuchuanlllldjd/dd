export type ID = number;
export interface User {
  id: ID;
  username: string;
  nickname: string;
  createdAt: string;
  gender: number;
  imgUrl: string;
  explain: string;
  phone: string;
  email: string;
  state?: number;
}

export enum Gender {
  "女" = 0,
  "男" = 1,
  "未知" = 2,
}
export interface Friend {
  state: number;
  user: User[];
}

export interface GetFriendListRes {
  items: User[];
}

export interface RegisterRes {
  message: string;
}
export interface LoginRes {
  token: string;
  isLogin?:boolean
}

export interface GetUserListParm {
  keyword: string;
  page: number;
  pageSize: number;
}

export interface LoginParams {
  username: string;
  password: string;
}

export interface UpdateUserParams {
  [propName: string]: string;
}
