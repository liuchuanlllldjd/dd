import { api, METHOD } from "@/utils/request";
import {
  GetFriendListRes,
  GetUserListParm,
  LoginParams,
  LoginRes,
  RegisterRes,
  User,
  ID,
  UpdateUserParams,
} from "./user.model";
//获取好友列表
export async function getFriendListApi(params: GetUserListParm) {
  return api<GetFriendListRes>(METHOD.GET, "/user", { params });
}
// 登录
export function loginApi(data: LoginParams) {
  return api<LoginRes>(METHOD.POST, "/auth/login", { data });
}

// 踢下线
export function kickOffApi(data: LoginParams) {
  return api<LoginRes>(METHOD.POST, "/auth/kickOff", { data });
}

//注册
export function registerApi(data: LoginParams) {
  return api<RegisterRes>(METHOD.POST, "/user/register", { data });
}

//根据id获取用户信息
export function getUserApi(id: ID) {
  return api<User>(METHOD.GET, `/user/${id}`);
}

//根据token获取用户信息
export function getUserinfoApi() {
  return api<User>(METHOD.GET, "/user/info");
}
//更新用户信息
export function updateUserApi(id: ID, data: UpdateUserParams) {
  return api(METHOD.PATCH, `/user/${id}`, { data });
}


//退出登录
export function logoutApi(id: ID) {
  return api(METHOD.POST, `/auth/logout/${id}`);
}

