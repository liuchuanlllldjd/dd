import { api, METHOD } from "@/utils/request";

// 获取临时cos token
export async function getCosToken(data?: any) {
  return api<any>(METHOD.POST, "/sts", { data });
}
