import { ID } from "../user/user.model";

export interface addTextMessageReq {
  userId: ID;
  friendId: ID;
  message: string;
}

export interface addTextMessageRes {}

export interface Message {
  id: ID;
  userId: ID;
  friendId: ID;
  message: string;
}
export interface getMessageListRes {
  items: Message[];
}

export interface getMessageListReq {
  friendId: ID;
  skip: number;
  take: number;
}

export interface getMessageRes {
  message: Message;
}
