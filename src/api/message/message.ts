import { ID } from "@/api/user/user.model";
import { api, METHOD } from "@/utils/request";
import {
  addTextMessageRes,
  addTextMessageReq,
  getMessageListRes,
  getMessageListReq,
} from "./message.model";
// 发送信息
export async function addTextMessageApi(data: addTextMessageReq) {
  return api<addTextMessageRes>(METHOD.POST, "/message", { data });
}

export async function getMessageListApi(params: getMessageListReq) {
  return api<getMessageListRes>(METHOD.GET, "/message", { params });
}
