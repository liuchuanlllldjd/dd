import { createSSRApp } from "vue";
import App from "./App.vue";
import Socketio from "./utils/socket.io";
import { painaInstall } from './stores'
// Toast
import 'vant/es/toast/style';

// Dialog
import 'vant/es/dialog/style';

// Notify
import 'vant/es/notify/style';

// ImagePreview
import 'vant/es/image-preview/style';

export function createApp() {
  const app = createSSRApp(App);
  app.use(painaInstall);

  app.use(Socketio, {
    connection: (import.meta.env.VITE_BASEURL + import.meta.env.VITE_SOCKET_PORT) || 'http://172.24.60.130:4000',
    options: {
      autoConnect: false, //关闭自动连接
      // ...其它选项
    },
  });

  return {
    app,
  };
}
