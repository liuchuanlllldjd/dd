import { defineStore, acceptHMRUpdate } from "pinia";
import { ref } from "vue";
import { showConfirmDialog } from 'vant';
import { User } from "../../api/user/user.model";
import { kickOffApi, loginApi, logoutApi } from "@/api/user/user";
import { LoginParams, ID, UpdateUserParams } from "@/api/user/user.model";
import { getUserApi, updateUserApi, getUserinfoApi } from "@/api/user/user";
import { Socket } from "socket.io-client";
const socket = inject("socket") as Socket;
export const useUserStore = defineStore(
  "user",
  () => {
    const user = ref<User>({
      username: "",
      nickname: "",
      email: "",
      explain: "",
      phone: "",
      gender: 2,
      createdAt: "",
      id: 0,
      imgUrl: "",
    });
    async function getUser() {
      if (unref(user).id)
        user.value = await getUserApi((unref(user) as User).id);
      else user.value = await getUserinfoApi();
    }

    async function getUserInfo() {
      user.value = await getUserinfoApi();
      console.log('to',user)
    }
    const friend = ref<User>();
    async function getFriendInfo(id: ID) {
      friend.value = await getUserApi(id);
      return friend;
    }

    const token = ref<string>("");
    async function login(userForm: LoginParams) {
      const { token: t,isLogin,...other } = await loginApi(userForm);
      if(isLogin) return isLogin
      if (!t) return;

      token.value = `Bearer ${t}`;
     await getUserInfo();
      uni.switchTab({
        url: "/pages/message/message",
      });
    }

    async function kickOff(userForm: LoginParams){
     const res= await kickOffApi(userForm);
     console.log(res)
    }

    async function logout() {
      await logoutApi(user.value.id)
      token.value = "";
      user.value={
        username: "",
        nickname: "",
        email: "",
        explain: "",
        phone: "",
        gender: 2,
        createdAt: "",
        id: 0,
        imgUrl: "",
      }
    }

    async function submitUser(data: UpdateUserParams, id?: ID) {
      await updateUserApi(id || (user.value as User)?.id, data);
      await getUser();
      uni.redirectTo({
        url: "/pages/user/editUser",
      });
    }

    const active = ref<string>("/pages/message/message");
    return {
      token,
      user,
      login,
      logout,
      getUser,
      getUserInfo,
      submitUser,
      friend,
      getFriendInfo,
      active,
      kickOff
    };
  },
  {
    persist: true,
  }
);
