import { ID } from "../api/user/user.model";
import AppHead from "@/components/AppHead/AppHead.vue";
import { getUserApi } from "@/api/user/user";
import { getStateApi, addFriendApi, passFriendApi } from "@/api/friend/friend";
import { User } from "@/api/user/user.model";
import { useUserStore } from "@/stores/modules/user";
export function useFriendOperation(id: ID) {
  const userStore = useUserStore();

  const friendState = ref<number>();

  const user = ref<User>();

  async function getUser() {
    user.value = await getUserApi(id);
    userStore.friend = user.value;
    console.log(user.value);
    const { state = 0 } = (await getStateApi({ friendId: id })) || {};
    friendState.value = state;
  }

  async function goAddFriend() {
    await addFriendApi({ friendId: id });
    await getUser();
  }

  async function pass() {
    await passFriendApi({ friendId: id });
    await getUser();
  }

  function logout() {
    uni.reLaunch({
      url: "/pages/login/login",
    });
    userStore.logout();
  }

  function goEditUser() {
    uni.navigateTo({
      url: "/pages/user/editUser",
    });
  }

  function goMessageHome() {
    uni.navigateTo({
      url: `/pages/message/home?id=${id}`,
    });
  }

  return {
    logout,
    goEditUser,
    friendState,
    goAddFriend,
    goMessageHome,
    pass,
    user,
    getUser,
  };
}
