import { PreQuest, create } from "@prequest/miniprogram";
import { ApiRes } from "@/api/types";
import { MiddlewareCallback, Context } from "@prequest/types";
import { useUserStore } from "@/stores/modules/user";
import { LoginParams } from "@/api/user/user.model";
// 全局配置
PreQuest.defaults.baseURL =
  import.meta.env.VITE_BASEURL +
    import.meta.env.VITE_DDAPI_PORT +
    import.meta.env.VITE_PREFIX || "http://172.24.60.130:3700/ddapi";
// import.meta.env.VITE_BASEURL || "http://172.24.60.130:3700/ddapi";
// 设置header
PreQuest.defaults.header = {};
const prequest = create(uni.request);

const refreshToken: MiddlewareCallback = async (ctx, next) => {
  // 这里将token放在pinia user模块中
const userStore = useUserStore(); 
  const token = userStore.token;
  if (ctx.request.header) {
    // header中统一设置token
    ctx.request.header["Authorization"] = token;
  }
  await next();
};

// 解析响应
const parse: MiddlewareCallback = async (ctx, next) => {
  await next();
  const { success, code, error } = ctx.response.data;
  if (!success) {
    uni.showToast({
      title: error.message,
      icon: "error",
    });
  }

  if (code === 401) {
    uni.navigateTo({
      url: "/pages/login/login",
    });
  }
};

// 实例中间件
prequest.use(refreshToken).use(parse);

type Method = "get" | "post" | "delete" | "put" | "patch" | "head" | "options";
export enum METHOD {
  GET = "get",
  POST = "post",
  DELETE = "delete",
  PUT = "put",
  PATCH = "patch",
  HEAD = "head",
  OPTIONS = "options",
}

interface Params {
  data?: any;
  params?: any;
}

export async function api<T>(
  method: Method,
  url: string,
  params?: Params
): Promise<T> {
  const { data } = await prequest[method]<ApiRes<T>>(url, params);
  return data.data;
}
